﻿#include<iostream>
using namespace std;
class Solution {
public:
    int countBeautifulPairs(vector<int>& nums) {
        int ans = 0, cnt[10]{};
        for (int x : nums) {
            for (int y = 1; y < 10; y++) {
                if (cnt[y] && gcd(y, x % 10) == 1) {
                    ans += cnt[y];
                }
            }
            while (x >= 10) {
                x /= 10;
            }
            cnt[x]++; // 统计最高位的出现次数
        }
        return ans;
    }
};

int main()
{
	return 0;
}