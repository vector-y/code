﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
using namespace std;

const int MAX_NAME_LENGTH = 100;
const int MAX_VERTICES = 1000;

// 边节点
struct EdgeNode {
    int adjvex; // 相邻顶点在顶点数组中的索引
    char relationship[MAX_NAME_LENGTH]; // 关系描述
    EdgeNode* next; // 下一个相邻顶点
};

// 顶点节点
struct VertexNode {
    char name[MAX_NAME_LENGTH]; // 顶点名称
    char country[MAX_NAME_LENGTH]; // 顶点所属国家
    int degree; // 顶点的度
    EdgeNode* firstEdge; // 第一个相邻顶点的指针
};

// 图结构
struct Graph {
    VertexNode vertices[MAX_VERTICES]; // 顶点数组
    int vexNum; // 顶点数量
    int edgeNum; // 边数量
};

// 创建图的函数
void createGraph(Graph& G, const char* filename) {
    ifstream file(filename);
    if (!file.is_open()) {
        cerr << "Error opening file " << filename << endl;
        exit(EXIT_FAILURE);
    }

    char name1[MAX_NAME_LENGTH], name2[MAX_NAME_LENGTH], relationship[MAX_NAME_LENGTH], country1[MAX_NAME_LENGTH], country2[MAX_NAME_LENGTH];
    int i = 0; // 当前顶点索引

    G.vexNum = 0;
    G.edgeNum = 0;

    while (file.getline(name1, MAX_NAME_LENGTH, ',') &&
        file.getline(name2, MAX_NAME_LENGTH, ',') &&
        file.getline(relationship, MAX_NAME_LENGTH, ',') &&
        file.getline(country1, MAX_NAME_LENGTH, ',') &&
        file.getline(country2, MAX_NAME_LENGTH)) {

        // 添加或查找顶点1
        int v1 = -1;
        for (int j = 0; j < G.vexNum; j++) {
            if (strcmp(G.vertices[j].name, name1) == 0) {
                v1 = j;
                break;
            }
        }
        if (v1 == -1) {
            if (i >= MAX_VERTICES) {
                cerr << "Too many vertices." << endl;
                exit(EXIT_FAILURE);
            }
            strcpy(G.vertices[i].name, name1);
            strcpy(G.vertices[i].country, country1);
            G.vertices[i].degree = 0;
            G.vertices[i].firstEdge = nullptr;
            v1 = i++;
            G.vexNum++;
        }

        // 添加或查找顶点2
        int v2 = -1;
        for (int j = 0; j < G.vexNum; j++) {
            if (strcmp(G.vertices[j].name, name2) == 0) {
                v2 = j;
                break;
            }
        }
        if (v2 == -1) {
            if (i >= MAX_VERTICES) {
                cerr << "Too many vertices." << endl;
                exit(EXIT_FAILURE);
            }
            strcpy(G.vertices[i].name, name2);
            strcpy(G.vertices[i].country, country2);
            G.vertices[i].degree = 0;
            G.vertices[i].firstEdge = nullptr;
            v2 = i++;
            G.vexNum++;
        }

        // 添加边（关系）
        EdgeNode* newNode = new EdgeNode;
        strcpy(newNode->relationship, relationship);
        newNode->adjvex = v2;
        newNode->next = G.vertices[v1].firstEdge;
        G.vertices[v1].firstEdge = newNode;

        // 更新度数
        G.vertices[v1].degree++;
        G.vertices[v2].degree++;

        // 递增边的数量
        G.edgeNum++;
    }

    file.close();
}

int main() {
    Graph G;
    const char* filename = "input.txt"; // 替换为实际的输入文件名

    createGraph(G, filename);

    cout << "Number of vertices: " << G.vexNum << endl;
    cout << "Number of edges: " << G.edgeNum << endl;

    return 0;
}
