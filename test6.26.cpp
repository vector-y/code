﻿#include<iostream>
#include<vector>
#include<algorithm>
#include<unordered_map>
using namespace std;
class Solution {
public:
    static const int mod = 1e9 + 7;
    int specialPerm(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> f(1 << n, vector<int>(n, -1));

        function<int(int, int)> dfs = [&](int state, int i) {
            if (f[state][i] != -1) {
                return f[state][i];
            }
            if (state == (1 << i)) {
                return 1;
            }
            f[state][i] = 0;
            for (int j = 0; j < n; j++) {
                if (i == j || !(state >> j & 1)) {
                    continue;
                }
                if (nums[i] % nums[j] != 0 && nums[j] % nums[i] != 0) {
                    continue;
                }
                f[state][i] = (f[state][i] + dfs(state ^ (1 << i), j)) % mod;
            }
            return f[state][i];
            };
        int res = 0;
        for (int i = 0; i < n; i++) {
            res = (res + dfs((1 << n) - 1, i)) % mod;
        }
        return res;
    }
};
class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        vector<int> dp(target + 1);
        dp[0] = 1;
        for (int i = 1; i <= target; i++) {
            for (int& num : nums) {
                if (num <= i && dp[i - num] < INT_MAX - dp[i]) {
                    dp[i] += dp[i - num];
                }
            }
        }
        return dp[target];
    }
};
class Solution {
public:
    bool isCompleteTree(TreeNode* root) {
        queue<TreeNode*>q;
        q.push(root);
        bool flag = false;
        while (!q.empty()) {
            auto node = q.front();
            q.pop();
            if (node == nullptr) {
                flag = true;
                continue;
            }
            if (flag) return false;
            q.push(node->left);
            q.push(node->right);

        }
        return true;
    }
};
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.size() == 0 || grid[0].size() == 0) {
            return 0;
        }
        int rows = grid.size(), columns = grid[0].size();
        auto dp = vector < vector <int> >(rows, vector <int>(columns));
        dp[0][0] = grid[0][0];
        for (int i = 1; i < rows; i++) {
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        }
        for (int j = 1; j < columns; j++) {
            dp[0][j] = dp[0][j - 1] + grid[0][j];
        }
        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < columns; j++) {
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
            }
        }
        return dp[rows - 1][columns - 1];
    }
};
class Solution {
public:
    unordered_map <int, int> m;
    int findLucky(vector<int>& arr) {
        for (auto x : arr) ++m[x];
        int ans = -1;
        for (auto [key, value] : m) {
            if (key == value) {
                ans = max(ans, key);
            }
        }
        return ans;
    }
};
class Solution {
public:
    int longestSemiRepetitiveSubstring(string s) {
        int ans = 1, left = 0, same = 0, n = s.length();
        for (int right = 1; right < n; right++) {
            if (s[right] == s[right - 1] && ++same > 1) {
                for (left++; s[left] != s[left - 1]; left++);
                same = 1;
            }
            ans = max(ans, right - left + 1);
        }
        return ans;
    }
};
int main()
{
	return 0;
}